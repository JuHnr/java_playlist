import java.util.ArrayList;
import java.text.DecimalFormat;

public class Music {
    // Titre
    private String title;  
    // Durée en secondes
    private int duration;
    // Artistes
    private ArrayList<Artist> artistSet; 

    // Constructeur
    public Music(String title, int duration, ArrayList<Artist> artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    // Getters et setters

    public String getInfos() {
        // Calculs pour convertir la durée au format mm:ss
        int durationMinutes = (int) this.duration / 60;
        int durationSeconds = this.duration - durationMinutes * 60;

        // Formatage des secondes et des minutes pour afficher deux chiffres systématiquement
        DecimalFormat df = new DecimalFormat("00");
        String durationSecondsFormat = df.format(durationSeconds);
        String durationMinutesFormat = df.format(durationMinutes);

        return "Title : " + this.title + "\nDuration : " + durationMinutesFormat + ":" + durationSecondsFormat + "\nArtist(s) : " + getArtistSet();
    }
    
    public String getArtistSet() {
        if (this.artistSet.size() > 1) {
            String artists = "";
            for (Artist artist : this.artistSet) {
                artists += artist.getFullName() + " - ";
            }
            return artists;
        }

        return this.artistSet.get(0).getFullName();
    }
    
    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
