import java.util.ArrayList;

public class Player {
    public static void main(String[] args) {

        /* Instanciations */
    
        // Artists
        Artist imagineDragons = new Artist("Imagine", "Dragons");
        Artist queen = new Artist("Queen", "");
        Artist freddieMercury = new Artist("Freddy", "Mercury");

        // Artist sets
        ArrayList<Artist> bohemianRhapsodyArtistSet = new ArrayList<Artist>();
        bohemianRhapsodyArtistSet.add(queen);
        bohemianRhapsodyArtistSet.add(freddieMercury);
      
        ArrayList<Artist> radioactiveArtistSet = new ArrayList<Artist>();
        radioactiveArtistSet.add(imagineDragons);

        ArrayList<Artist> sharksArtistSet = new ArrayList<Artist>();
        sharksArtistSet.add(imagineDragons);

    
        // Musics
        Music radioactiveMusic = new Music("Radioactive", 156, radioactiveArtistSet);
        Music bohemianRhapsodyMusic = new Music("Bohemian Rhapsody", 355, bohemianRhapsodyArtistSet);
        Music sharks = new Music("Sharks", 152, sharksArtistSet);

        // Playlist
        Playlist myPlaylist = new Playlist(new ArrayList<Music>());

        // Methodes tests
        myPlaylist.add(radioactiveMusic);
        myPlaylist.add(sharks);
        myPlaylist.add(bohemianRhapsodyMusic);


        System.out.println("\nMyPlaylist content: \n\n" + myPlaylist.getMusicList());
        
        System.out.println("Total duration: " + myPlaylist.getTotalDuration() + "\n");

        myPlaylist.remove(0);

        System.out.println("Music on position 3 removed. \nNew playlist duration : " + myPlaylist.getTotalDuration());

        myPlaylist.setCurrentMusic(bohemianRhapsodyMusic);
        System.out.println("\nThe current music is : \n" + myPlaylist.getCurrentMusic());


        myPlaylist.next();
        System.out.println("\nThe next music is : \n" + myPlaylist.getCurrentMusic());

    }
}
