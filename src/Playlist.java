import java.util.ArrayList;
import java.text.DecimalFormat;

public class Playlist {
    private Music currentMusic;
    private ArrayList<Music> musicList;

    // Constructeur
    public Playlist(ArrayList<Music> musicList) {
        this.musicList = musicList;
    }

    // Getters
    public String getMusicList() {
        String musics = "";
        for (Music music : musicList) {
            musics += music.getInfos() + "\n\n";
        }
        return musics;
    }

    public String getCurrentMusic() {
        return currentMusic.getInfos();
    }

    public void setCurrentMusic(Music music) {
        currentMusic = music;
    }

    // Methodes

    public void add(Music music) {
        musicList.add(music);
    }

    public void remove(int position) {
        musicList.remove(position);
    }

    public String getTotalDuration() {
        // Durée totale de la playlist en secondes
        int duration = 0;

        for (Music music : musicList) {
            duration += music.getDuration();
        }

        // Calculs pour convertir au format mm:ss
        int durationMinutes = (int) duration / 60;
        int durationSeconds = duration - durationMinutes * 60;

        // Formatage des secondes et des minutes pour afficher deux chiffres systématiquement
        DecimalFormat df = new DecimalFormat("00");
        String durationSecondsFormat = df.format(durationSeconds);
        String durationMinutesFormat = df.format(durationMinutes);

        return durationMinutesFormat + ":" + durationSecondsFormat;
    }

    /**
     * Passe à la musique suivante et la définit comme musique en cours. Si c'est la dernière musique de la liste, revient à la première.
     */
    public void next() {
        int currentMusicIndex = musicList.indexOf(currentMusic);
        if (currentMusicIndex < musicList.size()-1) {
            currentMusic = musicList.get(currentMusicIndex + 1);
        } else {
            currentMusic = musicList.get(0);
        }
    }
}
